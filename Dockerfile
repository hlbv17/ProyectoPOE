FROM mcr.microsoft.com/dotnet/sdk:5.0
COPY . /app
WORKDIR /app
RUN dotnet restore
##RUN dotnet build -c Release --no-restore
CMD ["dotnet", "run", "--project", "ProyectoPOE.csproj"]

